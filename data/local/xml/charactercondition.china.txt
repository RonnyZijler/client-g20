1	Poisoned
2	Strong Poisoned
3	Disease
4	Numb
5	Silence
6	Petrified
7	Coward
8	Outraged
9	Confused
10	Blind
11	Sleeping
12	Luck
13	Misfortune
14	Drunk
15	Cold
16	Deaf
17	Fool
18	Weak
19	Lazy
20	Lethargic
21	Gluttony
22	Camouflage
23	Blessed
24	Invisible
25	Not Tradable
26	Following
27	Chat Banned
28	View Cut Scene
29	stack(life,+,-5)
30	stack(life,+,-10);checkdead()
31	
32	
33	
34	
35	
36	
37	
38	
39	
40	
41	
42	say (*hic*|hiccup, 20);
43	say (Cough|Ahchoo, 20);stack(stamina,*,0.5)
44	
45	
46	
47	
48	
49	
50	
51	
52	
53	
54	
55	
56	
60	poison(0.5,1,5)
61	Ensemble
62	
64	Quick Summon
65	
66	
67	poison(0.4,1,5)
68	Increase in Aim Accuracy
69	Deadly
70	Potion Poisoning
71	
74	Cancel Dark Knight
77	stack(life,+,-1.0)
78	stack(life,+,-10.0)
79	PVP Penalty
80	Infected
81	
82	Weakened Attack
83	
84	Explode
85	Explode
86	
87	slow
89	
90	Leader's Blessing
91	Combat 2x EXP
92	Giggle Mushroom Cookie
93	Tear Noodles
94	Crazy Chocolate Balls
95	Conceited Steak
96	Gateau Chocolat of Ecstasy
97	
98	
99	
100	
101	
102	Poison Immunity
110	
111	
112	
113	
114	
115	
116	
117	
129	Petrification Immunity
136	Mana Usage Reduction
137	Stamina Usage Reduction
138	Explosive Resistance
139	Stomp Resistance
140	Mana Usage Increase
141	Stamina Usage Increase
142	Shrimp Fried Rice of Friendship
143	
148	
149	
150	
151	
152	Fire Shield
153	Ice Shield
154	Lightning Shield
155	Natural Shield
156	Move Slow
157	
158	Master Gatherer
159	
160	
161	
162	
163	
164	
165	
166	
167	
168	
169	
170	
171	
172	
173	
174	
175	
176	
177	
178	
179	
180	
181	
182	
183	
184	
185	
186	
187	
188	
189	
190	
191	
192	
193	
194	
195	
196	
197	You will not be poisoned.
198	You will not be petrified or get caught in yarn binding.
199	Mana usage will decrease when using the skill.
200	Stamina usage will decrease when using the skill.
201	Damage caused by explosion will decrease and you will not fall.
202	Damage caused by stomping will decrease and you will not fall.
203	Mana usage will increase when using the skill.
204	Stamina usage will increase when using the skill.
205	
206	
207	
208	
209	
210	
211	Increases gathering speed.
212	Charge
213	
214	
215	Attack Speed
216	
218	Increases the speed of melee weapons with less than average attack speeds and improves the aiming speed of long range weapons. 
219	달빛 이팩트 컨디션
220	
221	
222	Sulfur Poisoning
224	
225	poison(0, 1, 5)
226	Burn
227	
228	
229	Ice
230	
231	
232	별사탕
233	
234	
235	Mana Shield
236	
237	Uses Mana to reduce damage taken.
238	벚꽃놀이
239	
240	평소보다 경험치를 많이 받을 수 있다.
241	
242	
243	
244	Magic Casting Speed Boost
245	Weapon Attack Boost
246	
247	
248	Boosts your magic casting speed.
249	Boosts your weapon attack power.
251	
252	반투명하게 보이며 서로 공격할 수 없게 된다.
253	Combat EXP 1.1x
254	
255	
256	반투명
257	Combat 2x EXP
258	
259	
260	Bewildered
261	
262	
263	Elephant Shower
264	
265	
266	Curse
268	Receives a set amount of damage for a set period of time, and the overall movements will slow down considerably.
269	Blindness
270	
271	Blinds opponents and immobilizes or discombobulates them.
272	Ice - no effect
273	
274	
275	poison(0, 1, 1)
278	
279	
282	Production Skill Success Rate Changed
283	Production Skill Success Rate Adjusted
284	Item Proficiency Increased
285	Item Proficiency stats increased and same as a character 25 years of age
286	Cloud - Alchemy
287	
288	Cloud Effect
289	Bot-check failed.
290	
291	Condition the character is put into when bot-check fails.
292	Snow Storm Status
293	
294	Frozen after being hit with a snow storm
295	Doppelganger
296	
297	You've become a target of the Doppelganger.
298	Demigod
299	
300	Demigod Status
301	Doppelganger's Laugh
302	
303	The Doppelganger is laughing.
304	Doppelganger's Pain
305	
306	The Doppelganger is feeling pain.
307	발렌타인의 행복
308	발렌타인의 불행
309	
310	
311	발렌타인이 되어서 너무 행복하다.
312	발렌타인 따위는 왜 있는거냐!
313	Mirage
314	Fashion Show
315	
316	You are currently participating in the Fashion Show.
318	혀가 꼬임
319	
320	
322	말할때 혀가 꼬이는 증상이 생겼다.
323	거대상체
324	거대하체
327	
328	
329	
333	키가 커짐
334	키가 작아짐
335	
336	
337	
338	
339	Blessed Effect
340	Outraged Effect
341	
342	
343	
344	
345	Mini Potion Used.
346	
347	You've become small enough to fit through the small hole.
348	Unable to consume potions or food.
349	
350	You cannot consume potions or food.
351	Python Stone Barrier Status
352	
353	Python Stone Barrier
354	High Voltage
355	
357	checkdead
358	서서 하는 이펙트
359	
360	
361	Life Skill Training Boost
362	Battle Skill Training Boost
363	Magic Skill Training Boost
364	Alchemy Skill Training Boost
365	
366	
367	
368	
369	
370	
371	
372	
373	Name Color Change
374	
375	
376	Demigod STR increased
377	Demigod DEX increased
378	Demigod WILL increased
379	Demigod LUCK increased
380	Demigod INT increased
384	Demigod duration increased
385	Demigod cooldown decreased
387	Critical rate increased when Brionac is equipped
388	Magical Music effect increased
389	Movement speed increased
390	Demigod STR increased
391	Demigod DEX increased
392	Demigod WILL increased
393	Demigod LUCK increased
394	Demigod INT increased
398	Demigod duration increased
399	Demigod cooldown decreased
401	Critical rate increased when Brionac is equipped
402	Magical Music effect increased
403	Movement speed increased
404	Demigod Invulnerability
405	Demigod Invulnerability
406	Height Change
407	Movement speed increased
408	Tower Cylinder installed
409	Equips the Tower Cylinder to use an offensive alchemy skill.
410	Attacked by a Crow.
411	Attacked by a Crow.
413	Doppelganger suffers.\nThe boss goes to the next phase.
414	Demigod Fury of Light damage increased
415	Demigod Spear of Light damage increased
416	Demigod Spear of Shadow damage increased
417	Damage increased when Brionac is equipped
418	Continuously suffer a fixed amount of damage and Speed decreased.
419	Demigod Dark Storm damage increased
420	Demigod Spear of God damage increased
421	Demigod Spear of Shadow damage increased
422	Damage increased when Brionac is equipped
423	checkdead
424	누아자 페이즈 전환
425	누아자의 페이즈 전환
428	Allows you to attack while mounted.
429	Allows you to attack while mounted.
430	Decrease Stats Curse
432	Five of your stats are temporarily decreased.
436	Item Drop Rate Boost Status
437	Increases the item drop rate.
439	You'll receive a bonus when you complete a Shadow Mission.
440	Shadow Crystal
441	Item Drop Rate Boost Status
442	Increases the item drop rate.
443	Fishing Booster
444	Fishing Booster
445	Increases your chance of acquiring items that are normally hard to get.
446	Increases your chance of acquiring items that are normally hard to get.
447	채팅 색상 변경
448	Combat EXP Increase
449	Combat EXP increased based on Meta.
450	Damage Curse
451	Increased damage taken the next time you are hit.
452	Nuadha Set Item
453	Theatre Dungeon Spotlight
454	Designated stats are buffed.
455	Hamelin Contract
456	Lured by the sound of a flute
457	Out of Body
458	Spirit Clear
459	stack(life,%,-3); stack(mana,%,-3); stack(stamina,%,-3)
460	You were hit by the Grim Reaper's scythe. Your spirit has been separated from your body.
461	Occurs when the Grim Reaper takes a certain amount of damage.
462	Hands Full
463	Equipped with Shaved Ice
464	Demigod Shadow Spirit damage increased
465	Demigod Shadow Spirit damage increased
466	Troll Recovery
467	stack(life,%,5);
468	Trolls get enhanced HP recovery.
469	Smash Enhancement
471	Charge Enhancement
472	Ice Bolt Enhancement
473	Fire Bolt Enhancement
474	Healing Enhancement
475	Flame Burst Enhancement
476	Water Cannon Enhancement
477	Life Drain Enhancement
478	Magnum Shot Enhancement
479	Support Shot Enhancement
480	Fishing Enhancement
481	Refining Enhancement
482	Blacksmith Enhancement
483	Metallurgy Enhancement
485	Assault Slash Enhancement
486	Armor Bear Roar
487	Rosemary Gloves Equipped
488	Ice Magic Damage +15%
489	Flying Pet Boost
490	Flying Speed Increase, Turning Speed Decrease
492	stack(stamina,+,-1.0)
493	Claudius Pursuit
494	Translucent State, Stealthy Walk
495	Animal Character Training Kit
496	Movement speed increased with the Animal Character Training Kit.
497	Flying speed increased with the Animal Character Training Kit.
498	Today's Mission Completed
499	Goes up to Ladeca
500	Mabinogi Quiz Show Event
501	Pet Land Boost
502	stack(stamina,+,-2.0)
504	Currently transporting goods
505	Demigod Wings of Eclipse Enhancement
506	Demigod Wings of Rage Enhancement
507	Demigod Wings of Eclipse Enhancement
508	Demigod Wings of Rage Enhancement
509	마누스의 건강 비약
510	마누스의 비약을 마시고 강해진 상태
511	Movement speed increased
512	Movement speed increased
513	Movement Speed Increase, Gathering Speed Increase
514	Movement Speed Increase, Gathering Speed Increase
515	Weapon Seal Removal
516	Removes the hidden seal from a weapon.
517	Your body is becoming smaller.
518	You have become smaller and can now pass through small holes.
519	Reforging Movement Speed Boost
520	Reforging Trading Movement Speed Boost
521	Reforging Movement Speed Boost
522	Reforging Trading Movement Speed Boost
523	낚시용 의자 사용
524	낚시용 의자 사용
528	Grants higher compensation when selling Trade Goods
529	Letter of Guarantee
530	이동 속도 증가 포션 사용
531	이동 속도 증가 포션에 의한 이동 속도 증가
532	Magic Power Boost
533	Zombie Infection
534	Musical Petrification
535	Petrified by the music of desert giants.
536	Gold Strike Enhancement
538	Mode for pursuing Outlaws
539	Uppercut Debuff (Defense/Protection Down)
540	Defense/Protection Down
543	Speed Boost (Ducking Fighter)
544	Speed Boost (Ducking Fighter)
547	Pinned (Pummel)
548	Pinned (Pummel)
549	Dazed (Sommersault Kick/Cannot use Magic or Alchemy)
550	Dazed (Sommersault Kick/Cannot use Magic or Alchemy)
551	Outlaw Pursuit Mode
553	Received Alchemy Cloud Effect
554	Using Pummel
555	Using Pummel
556	Merrow's Curse
557	stack(mana,%,-2); stack(stamina,%,-2)
558	Merrow's Curse
559	Fighter Skill Training Boost
560	Respite Aftereffect
561	Respite Aftereffect
562	The decal is following the character
563	Popper Bomb received
564	Popper Bomb received
567	Movement Speed increased by music.
569	Music of the Harvest
570	Music of Peace
573	Music of Haste
574	Magical Music
575	Magic & Alchemy Charging Time decreased, Attack Speed increased.
576	Production & Gathering Success Rate increased, Gathering Speed increased.
577	Puts monsters to sleep.
578	Batter
579	Batter
583	poison(0,1,1)
584	수호의 영혼석 사용
585	수호의 영혼석으로 부활되어 잠시 반무적이 된다.
586	교역 불가 상태
587	교역 불가 상태
588	Music Skill Training Boost
589	Puppeteer Skill Training Boost
590	Flame of Resurrection
591	After using the Flame of Resurrection, you cannot use it again for a while.
592	Wire-Bound
593	Bleeding damage from being bound in puppetry string.
594	Wire-Bound and Immobilized
595	Bleeding damage from being bound in puppetry string.
596	Theatre Mission Crystal
597	Grants a bonus upon completing a Theatre Mission.
598	Bounty Hunter Mode
599	추적자 농장을 클리어하면 보너스를 받는다
600	Marionette Spirit
601	Getting attacked doesn't cancel skills.
602	Marionette Spiral Buff (Defense and Protection Increased)
603	Getting attacked doesn't cancel skills.
604	Buff you get right after using Spiral
605	Skill Stun
606	Stun caused by specific skills.
607	Flame of Resurrection
608	사망 시 자동으로 부활의 불꽃으로 부활한다.
609	사망 시 자동으로 부활의 불꽃으로 부활한다. 동시에 큰 충격파를 발생시킨다.
610	Bone Dragon Devil's Dash
611	Defense/Protection Down
612	Bone Dragon Devil's Cry
613	Movement Speed Down
614	Transformation Enhancement
615	Increased chance of obtaining monster transformation information.
616	Skill 2x EXP
617	Skill 2x EXP
618	Increases Damage
619	Decreases Damage
620	Decreases Defense/Protection
621	Increases Damage by %
622	Decreases Damage by %
623	Decreases Defense and Protection by %
624	Enhances Windmill
625	Tangled
626	Berserk
628	You are caught in a spider web and cannot move.
629	You are enraged, increasing your attack power and dulling any pain you feel.
631	Restful Wind
632	Increases Defense, Protection, and Mana and Stamina recovery speed.
633	Restful Wind's Embrace
634	Puts monsters to sleep.
635	Guard Increase
637	Increases Defensive and Protective stats by %.
639	Steadfast
640	You will not be knocked down even when hit.
641	Absorption
642	Increases Quest EXP
643	Raid Crystal
645	Increases Attack Power against Dragons
646	Increases Attack Power against the Red Dragon.
647	You will receive a bonus after completing the Field Quest Raid.
648	Contribution Bonus
649	Increases Contributions by 1,000 upon entering the summoned Field Quest
650	Way of the Gun
651	Critical and Attack Speed increase temporarily.
652	Dual Gun Skill Training Boost
653	Wing condition after use
658	Continuous Statue Damage
659	Continuous Statue Damage
660	Enhanced Assault Slash
667	Elemental Wave
668	Golden Time
669	When Elemental Wave is used
670	When Golden Time is used
671	Prepare to play a placed Instrument
672	Prepare to play a placed Instrument
673	Combat 2x EXP (Same Day Effect)
674	Life Skill Training Experience Increased (Same Day Effect)
675	More EXP from Shadow Missions (Same Day Effect)
676	The character will receive 2x Combat EXP for one day.
677	Life Skill Training Experience is increased. Effects only applied on the same day.
678	Doubles the amount of EXP awarded for completing a Shadow Mission. Effects only applied on the same day.
679	Electrocuted by the enhanced Shock skill
680	When electrocuted by the enhanced Shock skill
681	Received a passive effect of the Wind Alchemy
682	When the Wind Alchemy's passive effect is received
685	Abyss Curse
686	Abyss Curse
687	Bloody Duke
688	Duke's Temptation
689	Kissed in the Mist
690	Tempted by the Duke
691	Crisis Escape
692	Crisis Escape Skill Condition
695	인식 불가
696	특정 적에게 노출되지 않는 상태
697	나오의 축복(초보 펫 보너스)
698	초보자가 펫을 소환하면 펫에게 걸어주는 효과
699	Banshee Howl
700	Silent Voice
701	Damaged by Spooky Sounds
702	Unable to use skills
703	Beast Roar
704	Dance Time
705	ATT and Speed have increased
706	ATT and Physical DEF/Protection have increased
709	Ghost Madness
710	Color changes based on condition
711	Eastern Vampire Protection
712	Buffed by Eastern Vampire Protection
713	Halloween Channel Buff
714	Buffed after defeating monsters during the Halloween World Event
715	Halloween World Event Quest Check
716	Halloween World Event Defeat Quest Check Condition
717	As a reward for defeating the Returned Trickster, some of your stats will be greatly increased for 1 hour.
718	Flight Boost
719	Seize the Skies Flight Boost
720	Stun
721	Stunned Condition
722	Typhoon Boost
723	Seize the Skies Typhoon Boost
726	Commerce Speed
727	Commerce Speed
729	Doki Doki Island Campfire Effect
730	Lovey Dovey
731	Fly Together Preparation
732	Fly Together Preparation
739	Cooking Production EXP Boost
740	Cooking Production EXP Boost
741	이동 속도 증가
742	이동 속도 증가
743	-DEF
744	Defense Down
745	보스무적
746	맞아도 체력이 감소하지 않는다.
749	Possessed Ruairi Swng Skill DOT Damage
750	Possessed Ruairi Swng Skill DOT Damage
751	Shadow Mission EXP boost
753	Ninja Skill Training Boost
755	Increase movement and gathering speed
756	Increase HP, Wound, MP, Strength recovery rate
758	Increase HP, Wound, MP, Strength recovery rate
763	Remove Debuff
764	The debuff has been removed.
771	Contagion
772	Confinement
773	Condition given to player from 'The Holy Contagion' skill
774	Condition given to player from 'The Holy Confinement' skill
775	Refreshed
776	Fanaticism Buff
777	Binding Timing
778	Smiting Timing
779	Apostle Warding
780	Smiting Enhancement effect
781	Shield of Trust
782	Celestial Spike
783	Condition given to player from the caster(boss)'s Fanaticism skill.
784	Condition given to the caster(boss) from a Crusader skill.
785	Condition given to the caster(boss) from a Crusader skill.
786	Condition given to the caster (boss) as a default
787	Condition given to the caster(boss) from a Crusader skill.
788	Warding power of the Alban Knights
789	Binding power of the Alban Knights
790	Fatty
791	Your time during The Fattening has bulked you up to epic proportions.
792	Crusader Warding Power
793	Your warding power has been increased from divine strength.
794	Celestial Spike Divine Damage
795	Damage over time Condition from Crusader Warding skill
796	펫소환 불가 상태
797	Fanaticism Debuff
798	Condition given to player from the caster(boss)'s Fanaticism skill.
799	Meditation
800	Conditions depending on the use of Meditation skill
801	Perfect Pitch
802	Condition that prevents going off-key when using Hamelin's Tuner.
803	Incarnation
804	A temporary condition you get during the main Storyline. Crusader Skill cooldowns will be applied separately.
805	Aquarius Sign
806	Pisces Sign
807	Aries Sign
808	Taurus Sign
809	Gemini Sign
810	Cancer Sign
811	Leo Sign
812	Virgo Sign
813	Libra Sign
814	Scorpio Sign
815	Sagittarius Sign
816	Capricorn Sign
817	During the Aquarius sign period:\nItem Drop Rate x2, Item (Weapons) repair rate x2
818	During the Pisces sign period:\nQuest Completion EXP x2, Monster EXP x2
819	During the Aries sign period:\nProduction Success Rate +20%, Life Skill EXP x2
820	During the Taurus sign period:\nDucat Trade Profit x2, EXP x2, Gold x2,Merchant Rating x2
821	During the Gemini sign period:\nAP obtained when leveling up x2, AP obtained when leveling up Exploration x2
822	During the Cancer sign period:\nStrength +20, Intelligence +20, Dexterity +20, Will +20, Luck +20
823	During the Leo sign period:\nCombat EXP x2 (Close Combat, Magic, Battle Alchemy, Fighter)
824	During the Virgo sign period:\nCombat EXP x2 (Puppetry, Music, Transmutation)
825	During the Libra sign period\nQuest Completion Gold x2, Gold Drop x2
826	During the Scorpio sign period:\nCombat EXP x2 (Archery, Lance, Dual Gun, Ninja)
827	During the Sagittarius sign period:\nMin Damage +5, Max Damage +10, Magic Attack +40
828	During the Sagittarius sign period:\nDefense +10, Protection +10, Critical +10
829	Close Combat Bleeding Effect
830	Close Combat Dazed Effect
831	Close Combat Shatter Effect
832	Effect that is activated when using Smash with a blade-type Close Combat weapon equipped. Baseline bleed damage: 0
833	Effect that is activated when using Smash with a blunt-type Close Combat weapon equipped. Baseline dazed effect: 186
834	Effect that is activated when using Smash with an axe-type Close Combat weapon equipped. Baseline defensive decrease: 216
837	Bash Combo
838	(Tentative)Condition that the Bash 20019 Skill gives\nCounts every attack and increases damage
839	Bash Combo Max
840	Damage Curse 2
841	(Tentative)Condition that the Bash 20019 Skill gives\nCondition given at max combo
842	Same condition as the Support Shot 21006 condition [129. Damage Curse]. Created it separately to allow it to overlap.
843	Enhanced Critical Damage
844	Enhanced Bash
845	Decreased Durability Loss
846	Sleeping Wool
847	Giving Off the Charm
848	Movement speed decreases after falling asleep.
849	Charmed and immobilized.
850	Light as Wool
851	Increases your movement speed.
852	Ice Spear Deep Freeze
853	Frozen\nYour damage is reduced while this effect lasts.
854	Icebolt Hobble
855	Speed reduced\nYour speed is reduced while this effect lasts.
856	Increases HP
857	Increases Max Damage
858	Increases HP Regeneration Rate
859	MP Regeneration Rate increased
860	Increases Stamina Regeneration Rate
861	Critical Rate Boost
862	Increases Training Experience for particular talent skills
863	Increases Close Combat Talent Skill damage
864	Increases Magic Talent Skill Casting Speed
865	Increases Archer Talent Aim Speed
866	Increases Gathering Speed and success rate
867	Sheep Wolf World Quest check
868	Sheep Wolf World Event defeat condition
869	Ranged aiming speed increased
870	Urgent Shot effect. Aiming speed for all Archery skills increased.
871	Binding
872	You cannot move or use movement skills.
882	God Hand: Twelve Labors
884	Smokescreen decreases Defense and Protection
885	Defense/Protection Down
886	Soul Linked
888	Soul Linked
890	Cooking Training EXP Boost
891	Cooking Training EXP Boost
892	Recipe Quality Boost
893	Cooking Buff Duration Increased
894	Cooking Training EXP Boost
895	Cooking Training EXP Boost
896	Recipe Quality Boost
897	Cooking Buff Duration Increased
898	Judgment Blade Enhancement
899	Judgment Blade Enhancement
900	Lance Charge Enhancement
902	Lance Charge Enhancement
903	Fallen Fairy Summon Skill
904	Fallen Fairy Summon Skill
905	Fallen Fairy Summon Skill Movement Speed Buff
906	Fallen Fairy Summon Skill Attack Speed Buff
907	Fallen Fairy Summon Skill Movement Speed Buff
908	Fallen Fairy Summon Skill Attack Speed Buff
909	the Returned
910	The character will receive Skill 2x EXP. When clearing dungeons, Shadow Missions, or Theatre Missions, party members receive 1.5x EXP bonus.
911	풍등 날리기
912	풍등을 날리는 중
913	Craft Success Rate Enhanced
914	Adds a success rate bonus when using a craft skill
915	Safeguard
916	An exclusive Succubus Queen status effect that gets stacks every time she hits you
917	Berserk Phantom
918	Phantom's Roar
919	An exclusive Wraith status effect. When it becomes active, the enemy does not stagger when hit, and Attack, Magic Attack, Defense, Protection, Magic Defense and Magic Protection all increase.
920	A condition for the Phantom's Roar skill. When applied, pets cannot be summoned.
921	Captive Audience
922	The performance you're observing is so captivating that you find yourself unable to perform certain disruptive actions. This is similar to when you participate in OX quizzes but even more strict.
923	Soul Rift
924	User's condition when the Succubus Queen casts Soul Rift
925	Combat 2x EXP (Event)
926	Grants 2x EXP during some events.
927	Cannot be revived by the balloon.
928	Auto-revive not available even when equipped with a balloon.
929	Stealth Soul
930	This takes effect when Soul Rift is used. Any enemy you are currently engaged with will stop targeting you, and no other enemies will target you unless you strike first.
931	Doki Doki Wig Condition 1
932	Doki Doki Wig Condition 2
933	Doki Doki Wig Condition 3
934	Effects take place during the Doki Doki Island event.\nStrength +20, Intelligence +20, Dexterity +20, Will +20, Luck +20
935	Effects take place during the Doki Doki Island event.\nMin Damage + 5, Max Damage +10, Magic Attack +40
936	Effects take place during the Doki Doki Island event.\nDefense +10, Protection +10, Magic Defense +10, Magic Protection +10, Critical +10
939	Boss Monster Hit Check Condition
940	World Event Boss Monster Hit Check Condition
941	Scooter Imp Summon Skill
942	Scooter Imp Summon Skill
943	Tiny Jibes
944	Tiny Jibes
945	Pet Combat 2x EXP
947	The Pet will receive Combat 2x EXP.
948	Speed Increase
949	Requirements for increased speed from totem
950	Shooting Gallery
951	Shooting Gallery Conditions
952	Play Music Box
953	Music Box ON/OFF status
954	Great Mood
955	Increases All Stats
956	Increases Speed
957	Increases Attack Delay
958	Increases Max Damage
959	Increases Magic ATT
960	A condition of the Festia ranking reward wings. Can be stacked with other conditions.
961	A condition of the Festia ranking reward wings. Can be stacked with other conditions.
962	A condition of the Festia ranking reward wings. Can be stacked with other conditions.
963	A condition of the Festia ranking reward wings. Can be stacked with other conditions.
964	No Summon, No Skill
965	A condition where you are unable to use skills or summon pets.
966	Immune to DoT Damage
967	A condition where your HP is unaffected by DoT damage and/or Modify Life.
968	Decreases Defense/Protection
969	Defense/Protection Down
970	Melody Step
971	Mezzo-Forte
972	Psychomancy
973	Haunted House Event Skill condition. Psychomancy reflects the caster's suffering, increasing damage and attack.
974	Blessed Melody Step
975	Mezzo-Forte decreases the durability loss of your items.\nMusic Buff Effect +3.
976	Recovery Potion Effect Bonus
977	Exploration EXP Bonus
978	Huge Lucky Bonus
979	Stats Boost
980	Increases the effectiveness of potions.
981	Increases the amount of Exploration EXP gained.
982	Triggers the Huge Lucky Effect in combat or gathering.
983	Increases the values of certain stats.
984	Production EXP Bonus
985	Increases the amount of Production EXP gained.
986	Combat EXP x4
987	You gain Combat EXP x4.
988	MP Consumption Decrease
989	Stamina Consumption Decrease
990	Critical Increase
991	Decreases MP consumption of skill preparation by 5%
992	Decreases Stamina consumption of skill preparation by 10%
993	Increases Critical damage by 5
994	Use Imp Mini Potion
995	The imp magic shrank you!
996	Breath of Ladeca
997	Vision of Ladeca
998	Might of Ladeca
999	You received the blessings of Ladeca. Speed increases.
1000	You received the blessings of Ladeca. Aiming Rate and Aiming Speed increases for ranged attack.
1001	You received the blessings of Ladeca. Attack Speed increases, and additional damage is applied upon using close combat talent skills with close combat weapons.
1002	신성 스킬 수련 증가
1003	Dance of Death
1004	Dance of Death Debuff
1005	Applied to character while using the Dance of Death skill.
1006	Applied to monsters hit by the Dance of Death.
1007	Shadowlike
1008	Bleeding
1009	You can avoid being recognized by certain monsters.
1010	Bleeding consumes Strength. The more Bleeding is stacked, the more Strength it consumes.
1011	Menagerie Summon Skill Attack Speed Buff
1012	Menagerie Summon Skill Speed Buff
1013	Increase Attack Speed for a limited time
1014	Increase Speed for a limited time
1017	Interrupted Connection
1018	Temporarily cuts off ties to your pet.
1021	Peeved
1022	You are provoked into a fury. Increase Speed and ATT.
1027	Exploration EXP Bonus
1028	Gain bonus Exploration EXP while wearing this set.
1029	Homestead Stone Mine Owner
1030	Receive Homestead Stones upon logging in every day.
1031	Cannot move
1032	Cannot move character or monster
1033	Acquire Bachram
1034	Death Mark
1035	Bachram Status
1036	Death Mark Debuff. Increases attack damage and attracts nearby monsters.
1037	Chain Burst
1038	Increases the effect of Dorcha skills for a brief time.
1039	Bachram Boost
1040	Quickly builds Dorcha for a brief time.
1041	Shadow Mission Monster EXP Bonus
1042	You can get more EXP by defeating monsters in Shadow Missions.
1043	Chain Slash Skill Training Boost
1044	Receiver of Nao's Blessing
1045	The character will receive Skill 2x EXP. When clearing dungeons, Shadow Missions, or Theatre Missions, party members receive 1.5x EXP bonus.
1046	Let's go nuts and have an adventure!
1047	Detective Squirrel is away on an investigation, darting off after every single clue. Good thing you gave the squirrel a bento box for lunch! What sort of adorable mystery is being investigated right now? We'll never know...
1048	Meditation Potion
1049	If you Rest continuously, you will gain EXP at each full elapse of the set number of minutes.
1050	Crusader 2x EXP
1051	Music Buff Skill Effect Boost
1052	The character will receive Crusader 2x EXP.
1053	Music Buff Skill Effect +2.
1054	Dorcha Leech Amount Increase
1055	Death Mark Damage Increase
1056	Eiren's Outfit Set Effect. Dorcha leech amount is increased.
1057	Eiren's Outfit Set Effect. Death Mark damage is increased.
1058	Attack Delay Reduction
1059	Your delay between attacks is reduced, and so is your recovery from being stunned, AND the time it takes you to start charging a special skill.
1060	물병자리
1061	물고기자리
1062	양자리
1063	황소자리
1064	쌍둥이자리
1065	게자리
1066	사자자리
1067	처녀자리
1068	천칭자리
1069	전갈자리
1070	사수자리
1071	염소자리
1072	유효 시간 동안 활성화 시 적용 효과\n아이템 드랍률 2배 증가, 아이템 수리율 증가 (100%)
1073	유효 시간 동안 활성화 시 적용 효과\n퀘스트 완료 경험치 2배 증가, 몬스터 경험치 2배 증가
1074	유효 시간 동안 활성화 시 적용 효과\n생산 스킬 생산 성공률 20% 증가, 생활 재능 스킬 수련치 2배 증가
1075	유효 시간 동안 활성화 시 적용 효과\n교역 시 두카트 차익 2배 증가, 경험치 2배 증가, 골드 2배 증가, 신용도 2배 증가
1076	유효 시간 동안 활성화 시 적용 효과\n레벨업 시 획득 AP 2배 증가, 탐험 레벨업 시 획득 AP 2배 증가
1077	유효 시간 동안 활성화 시 적용 효과\n체력, 지력, 솜씨, 의지, 행운 20씩 증가
1078	유효 시간 동안 활성화 시 적용 효과\n근접 전투, 마법, 전투 연금술, 격투술 재능 스킬 수련치 2배 증가
1079	유효 시간 동안 활성화 시 적용 효과\n인형술, 음악, 연성 연금술, 체인 슬래시 재능 스킬 수련치 2배 증가
1080	유효 시간 동안 활성화 시 적용 효과\n퀘스트 완료로 획득하는 골드 2배 증가, 골드 드랍 2배 증가
1081	유효 시간 동안 활성화 시 적용 효과\n궁술, 랜스, 슈터, 닌자 재능 스킬 수련치 2배 증가
1082	유효 시간 동안 활성화 시 적용 효과\n최소 대미지 5증가, 최대 대미지 10증가, 마법 공격력 40증가
1083	유효 시간 동안 활성화 시 적용 효과\n방어, 보호, 마법 방어, 마법 보호, 크리티컬 10씩 증가
1084	Prism
1085	Blue Prism
1086	Collect EXP in the Empty Prism.
1087	The Prism Effect grants Skill 2x EXP.
1090	Loving Bond
1091	Party Buff
1092	Loving Bond Item Effect Activated
1093	Party Buff Item Effect Activated
1094	Orange Prism
1095	This prism effects doubles the amount of AP you gain upon character or exploration level up.
1096	Yellow Prism
1097	하늘빛 프리즘
1098	The Prism Effect grants Combat 2x EXP.
1099	프리즘 효과. 그림자 미션 경험치가 2배 증가한다.
1122	인형 동행 보너스
1123	인형 동행 보너스
