0	Caster-in-Training Wig Box
1	Hurry, or wolves will get all the animals!
2	Red Bears are violent animals. Be careful!
3	Fomors are scaring the Townspeople. Help out!
4	The Shadow Realm is dangerous. Please help.
5	White Spiders make cobweb gathering hard.
6	I'm so tired of those mimics...bats too!
7	Fomors plague so many towns. Please help.
8	Deian says it's important to help a friend in need.
9	Spiders are really only good for cobwebs...
10	All that wheat will rot if it's not harvested...
11	Erinn has a severe firewood shortage.
12	Herbs are crucial for medicine. Stay stocked!
13	Some farmers asked us to help harvest corn.
14	Those berries are more elusive than expected...
15	This calls for someone with real stamina.
16	You can use firewood you chopped last week!
17	It's good to stockpile Wheat Flour just in case.
18	This is cake for someone good with their hands.
19	It's always a good idea to have potions ready.
20	If it's too difficult, you can try again tomorrow.
21	Few who claim to be good cooks actually are.
22	Just let the rhythm flow through you.
23	It's important to help those in need.
24	We urgently need some Trade Goods delivered.
25	We need to clear the roads to keep trading.
26	There's no place like Homestead!
27	Slevin has a special request for you...
28	The Awakening of Light skill will make this easy.
29	It's a bit murder-y, but I like your enthusiasm!
30	I wonder how bear-steak tastes...
31	Great, kid. Don't get cocky.
32	Hmm... Are you sure you didn't have help?
33	I'm glad you didn't mind trivial chores.
34	They're really gone? I'm so glad to hear it!
35	You already got them? Fomors are a bit weak...
36	You're pretty good with your hands.
37	Nice work. Nobody like a dirty town.
38	Looks like you reaped what they sowed? Heh.
39	Wow, you're really doing everything I ask!
40	You're a pretty hard worker.
41	Mmm...Corn.
42	Everyone knows fruit doesn't count on a diet.
43	Wow! You've gone above and beyond!
44	Lumber can be used for all sorts of things.
45	I'd be happy to take that off your hands.
46	With enough resolve, nothing can stop you!
47	If you don't need it now, you can save it for later.
48	I can see I had nothing to worry about.
49	You're not gonna eat all that yourself, are you?
50	Next time you should go for a 5-combo streak!
51	You can never be too prepared.
52	I'm glad those traders could sell their wares.
53	Fewer bandits means safer roads.
54	How did it go?
55	You're a Jack-of-all-trades, aren't you?
56	Did you find anything worthwhile?
